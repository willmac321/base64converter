#!/usr/bin/env python3

import sys
import getopt

BASE64_TABLE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

def decode(base):
    out = ''
    bits = ''
    pad = base[-2:].count("=")

    if (pad > 0):
        base = base[:-1 * pad]

    for i in base:
        bits += f'{BASE64_TABLE.index(i):06b}'

    if (pad > 0):
        bits = bits[:-2 * pad]

    for j in range(0, len(str(bits)[2:]) - 1, 8):
        out += chr(int('0b'+bits[j:j+8],2))

    print(out)
    return out

def encode(string):
    out = ''
    bits = get_bits_from_string(string) 
    #find padding amount for sextet encoding
    bits, pad = add_padding(bits)
    for i in range(0, len(bits) - 1, 6):
        x = int(''.join(map(str,bits[i:i+6])), base=2) 
        out += BASE64_TABLE[x]
    out += ('=' * int(pad / 2))
    
    print(out)
    return out

def add_padding(bits):
    byte_n = int(len(bits) / 8)
    pad = 6 - ((len(bits) % 6) )
    if pad == 6:
        pad = 0
    zero = int(0)
    bits.extend([zero] * pad)

    return bits, pad

def get_bits_from_string(string):
    res = []
    for c in string:
        bits = bin(ord(c))[2:]
        bits = '00000000'[len(bits):] + bits
        res.extend([int(b) for b in bits])
    return res

if __name__=="__main__":
    opts, args = getopt.getopt(sys.argv[1:], "e:d:")
    for o, a in opts:
        if o == "-e":
            encode(a)
        elif o == "-d":
            decode(a)

